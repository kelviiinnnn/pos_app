<?php session_start(); error_reporting(0);?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="../../resource/css/sweetalert.css" rel="stylesheet">
 	<link href="../../resource/css/theme/twitter.css" rel="stylesheet">
</head>

<body>
   <script src="../../resource/js/main/jquery.min.js"></script>
   <script src="../../resource/js/main/sweetalert.min.js"></script>
</body>
</html>

<?php
include 'db.php';
require 'csrf.php';
CSRF::init();

if(isset($_REQUEST['logout'])){	
	if(!CSRF::validatePost()) {
		unset($_SESSION['limit']);
		session_destroy();
		die('<script>
			swal({title: "Warning",text: "Access Denied!",type: "warning"}, 
			function() {window.location = "../";
			});
		</script>');			
	}
	unset($_SESSION['limit']);
	unset($_SESSION['nama']);
	session_destroy();	
	echo '<script>
				swal({title: "Success",text: "Logout Success!",type: "success"}, 
				function() {window.location = "../";
				});
		</script>';	
}else{
	unset($_SESSION['limit']);
	session_destroy();
	die('<script>
			swal({title: "Warning",text: "Access Page Denied!",type: "warning"}, 
			function() {window.location = "../";
			});
		</script>');
}
?>