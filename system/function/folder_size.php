<?php
//untuk menghitung size total sebuah folder
function folderSize ($dir){
	$size = 0;
	foreach (glob(rtrim($dir, '/').'/*', GLOB_NOSORT) as $each) {
		$size += is_file($each) ? filesize($each) : folderSize($each);
	}
	return $size;
}
?>