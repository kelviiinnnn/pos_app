<div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
				<h4 class="modal-title" id="">Create Account</h4>
		</div>
		<div class="modal-body">
			<form class="form-horizontal form-label-left">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Username:</label>
					<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
					<input type="text" id="user_nama" class="form-control" placeholder="Input Username" required autofocus onblur="this.value=removeSpaces(this.value);">
					</div>						
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Password:</label>
					<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
					<input type="password" id="pass" class="form-control" placeholder="Input Password" required autofocus>
					</div>						
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Name:</label>
					<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
					<input type="text" id="nama" class="form-control" placeholder="Input Name (Ex : Mr. Sanda)" required autofocus>
					</div>						
				</div>
				<div class="modal-footer">       
					<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
					<button type="button" class="btn btn-success" data-dismiss="modal" onclick="create_user();">Create</button>
				</div>
			</form>
		</div>
	</div>
</div>