<?php session_start(); error_reporting(0);?>
<?php include "../../system/_core/db.php";?> 
<?php include "../header.php";?>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">


	 
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
			
            <div class="x_panel">
                <div class="x_title">          
					<h2>Data Account User</h2>
					<input id="nama-dokumen" type="hidden" value="Data Account User">
					<div class="clearfix"></div>
                </div>
            <div class="x_content">
                <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target=".tambah" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus-square" ></i> Tambah Data</a>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover dataTables-dashboard">
						<thead>
							<tr>
								<th>Username</th>                       
								<th>Nama</th>
								<th>Role</th>
								<th>Status</th>
								<th style="width:280px;">Action</th>
							</tr>
						</thead>
					<tbody>
						<?php
						// WHERE tabel_id NOT IN (216)
						$selek_data = mysqli_query($koneksi, "SELECT * FROM data_user");
						while($data = mysqli_fetch_array($selek_data)){
							?>                  
						<tr>
							<td><?php echo $data['user_nama']; ?></td>
							<td><?php echo $data['nama']; ?></td>
							<!--<td><?php echo $data['role']; ?></td>-->
							<td>
								<?php if($data['role'] == 99){
								echo 'Owner';
								}else if($data['role'] == 66){
								echo 'Admin';
								}else{
									echo 'User';
								}						
								?>						
							</td>
							<td>
								<?php if($data['status'] == 1){
									echo 'Active';
								}else{
									echo 'Suspend';
								}						
								?>						
							</td>
							<td>
								<a id="edit_link" href="#" class="btn btn-success btn-xs last" data-toggle="modal" data-target=".edit" data-backdrop="static" data-keyboard="false" style="margin-bottom:0px;float:left;"
								data-tabel_id="<?php echo $data['tabel_id']; ?>"
								data-nama="<?php echo $data['nama']; ?>"
								data-role="<?php echo $data['role']; ?>"><i class="fa fa-pencil"></i> Edit</a>	
								<form id="form-repas<?php echo $data['tabel_id']; ?>" method="post" action="../../system/user_panel/repas.php" style="float:left;">
									<input type="hidden" name="tabel_id" value="<?php echo $data['tabel_id']; ?>">
									<input id="module" type="hidden" name="module" value="user_panel">
									<?php print CSRF::tokenInput(); ?>
									<button class="btn btn-info btn-xs last" onclick="get_nama_repas('form-repas<?php echo $data['tabel_id']; ?>')"><i class="fa fa-refresh"></i> Reset Password</button>
								</form>
								<?php if($data['status'] == 1){									
										include "suspend.php";
									}else{
										include "active.php";
									}						
								?>					
							</td>
						</tr>
								<?php
								}
								?>					  
                    </tbody>
                </table>
			</div>
        </div>
	</div>
			
			<?php include "modal.php";?> 	

</div>
    </div><!-- right row -->
	  
	  <?php include "../footer.php";?> 	