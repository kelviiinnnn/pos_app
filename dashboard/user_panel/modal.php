<div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="">Tambah Account User</h4>
            </div>
            <div class="modal-body">
				<form method="POST" action="../../system/user_panel/create.php" class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Nama:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" name="nama" class="form-control" placeholder="Masukan Nama Account" required autofocus maxlength="30" minlength="4">
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Username:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
						<input type="text" name="user_nama" class="form-control" placeholder="Masukan Username Login" required autofocus maxlength="16" minlength="4" onblur="this.value=removeSpaces(this.value);">
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Role:</label>
						<div class="col-md-5 col-sm-5 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<select name="role" class="form-control">
								<option value="user">User</option>
							</select>
						</div>						
					</div>

						<?php print CSRF::tokenInput(); ?>
            </div>
			<div class="modal-footer">
						<input id="module" type="hidden" name="module" value="user_panel"> 
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Keluar</button>
						<button type="submit" class="btn btn-success">Tambah</button>
			</div>
				</form>
		</div>
	</div>
</div>
				

				<!-- Modal untuk Edit -->
<div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Edit Data Account User</h4>
            </div>
            <div class="modal-body">
				<form method="POST" action="../../system/user_panel/edit.php" class="form-horizontal form-label-left">
					<input id="tabel_id" type="hidden" name="tabel_id"> 
					<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Nama:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="nama" name="nama" class="form-control" placeholder="Masukan Nama Account" required autofocus maxlength="30" minlength="4">
							</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Role:</label>
						<div class="col-md-5 col-sm-5 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<select name="role" id="role" class="form-control">
								<option value="23">User</option>
							</select>
						</div>						
					</div>
						<?php print CSRF::tokenInput(); ?>
			</div>
			<div class="modal-footer">
				<input id="module" type="hidden" name="module" value="user_panel"> 
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Keluar</button>
						<button type="submit" class="btn btn-success">Simpan</button>
			</div>
                </form>
        </div>
    </div>
</div>
		
		<script src="modal.js"></script>
		<script src="../../resource/js/custom/custom-tabel.js"></script>		
		<script src="../../resource/js/custom/confirm-suspend.js" type="text/javascript"></script>
		<script src="../../resource/js/custom/confirm-repas.js" type="text/javascript"></script>
		<script src="../../resource/js/custom/confirm-actived.js" type="text/javascript"></script>