<?php  session_start();require '../../system/_core/csrf.php';CSRF::init();
if(isset($_SESSION['limit'])&& isset($_SESSION['nama']))
{
	$limit = $_SESSION['limit'];
	$nama = $_SESSION['nama'];
	$user_id = $_SESSION['tabel_id'];
	$role = $_SESSION['role'];
	if (time() < $limit){		
	}else{
		unset($_SESSION['limit']);
		session_destroy();
		die("<script>window.location = '../../system/_core/konfirmasi-akses.php'</script>");		
	}
}else{
	die("<script>window.location = '../../system/_core/konfirmasi-akses.php'</script>");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta, title, favicons -->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Fhaz Framework</title>
  <script src="../../resource/js/head-menu.js"></script> 
  </head>
<body class="nav-md">

<div class="container body">
    <div class="main_container">
		<div class="col-md-3 left_col">
			<div class="left_col scroll-view">
				<div class="navbar nav_title" style="border: 0;">
					<a href="../../home/main/" class="site_title"><i class="fa fa-gg-circle"></i> <span>FHAZ</span></a>
				</div>
				<div class="clearfix"></div>
					<!-- menu prile quick info -->
					<div class="profile">
						<div class="profile_pic">
							<img src="../../resource/img/user.jpg" alt="..." class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<h3 style="margin-bottom:5px;">Welcome,</h3>
							<h3><?php echo $nama?></h3>
							<h3 style="margin-top:5px;"><?=date('d - M - Y')?></h3>
							<input type="hidden" id="role_id" value="<?php echo	$role;?>">
						</div>
					</div>
					<!-- /menu prile quick info -->
					<br />
          
					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<br> <div class="clearfix"></div>
								<ul class="nav side-menu">
									<li><a href='../main/'><i class='fa fa-home'></i> Main <span class='fa fa-chevron-down'></span></a></li>
									<li><a><i class="fa fa-user"></i> User Panel <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu" style="display: none">
											<li><a href='../user_panel/'>Login User</a></li>
										</ul>
									</li>
										  <!--<?php 
											if($role != 33){
											 echo "";
											}
											if($role == 212){
											 echo "";
											 echo "";
													 echo "";
											}
										  ?>-->
								</ul>
						</div>
					</div>
						<!-- /sidebar menu -->
			</div>
</div>

<!-- top navigation -->
<div class="top_nav">

    <div class="nav_menu">
        <nav class="" role="navigation">
			<div class="nav toggle">
				<a id="menu_toggle"><i class="animated bounceIn fa fa-bars"></i></a>
			</div>
            <ul class="nav navbar-nav navbar-right">
				<li class="">
					<a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						Setting <span class="fa fa-gear fa-lg" style="display:inline-block !important;"></span>
					</a>
					<ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
						<li><a  data-toggle="modal" data-target=".ganti_pass" data-backdrop="static" data-keyboard="false" style='padding-left:15px;'>  Change Password</a>
						</li>
						<li>
							<form action="../../system/_core/logout.php" method="post" style="width: auto;">                      
								<input id="logout" type="submit" name="logout" value="Log Out">
								<?php print CSRF::tokenInput(); ?> 
							</form>
						</li>
					</ul>
				</li>
			  
				<div class="animated bounceInUp modal fade ganti_pass" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-md">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
								</button>
								<h4 class="modal-title" id="">Change Password</h4>
							</div>
							<div class="modal-body">
								<form method="POST" action="../../system/_core/change-pass.php" class="form-horizontal form-label-left">
								<input type="hidden" name="tabel_id" value="<?php echo $user_id;?>">
								<div class="form-group">
									<label class="control-label col-md-4 col-sm-4 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">New Password:</label>
									<div class="col-md-8 col-sm-8 col-xs-12"  style="padding-left:0px;padding-right:0px;">
									<input type="password" name="password" class="form-control" placeholder="Input New Password" required maxlength="10" minlength="6" autofocus>
								</div>						
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 col-sm-4 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Confirmation Password:</label>
									<div class="col-md-8 col-sm-8 col-xs-12"  style="padding-left:0px;padding-right:0px;">
									<input type="password" name="konfirm" class="form-control" placeholder="Input Same Password" required maxlength="10" minlength="6">
									</div>						
								</div>
								<?php print CSRF::tokenInput(); ?>
							</div>
							<div class="modal-footer">
								<input id="module" type="hidden" name="module" value="data_user"> 
								<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
								<button type="submit" class="btn btn-success">Save</button>
							</div>
								</form>
						</div>
					</div>
                </div>
            </ul>
        </nav>
    </div><!-- nav menu -->
</div><!-- top navigation -->   